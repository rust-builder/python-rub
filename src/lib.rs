//! Rust Builder for [Python](http://python.org).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub python --help
//! python - Rust Builder
//!
//! Usage:
//!     rub rust [options] [&lt;lifecycle&gt;...]
//!     rub rust (-h | --help)
//!     rub rust --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;        Set the projects directory.
//!     -b --branch &lt;branch&gt;  Set the build branch. [default: master]
//!     -t --enable-test      Enable tests.
//!     -p --prefix &lt;prefix&gt;  Set the installation prefix.
//!     -u --url &lt;url&gt;        Set the SCM URL.
//!     -h --help             Show this usage.
//!     --target &lt;target&gt;     Target triples to build.
//!     --force-configure     Force the configure lifecycle to run.
//!     --version             Show python-rub version.
//! </pre>
//!
//! # Examples
//! ```rust
//! # extern crate buildable; extern crate python_rub; fn main() {
//! use buildable::Buildable;
//! use python_rub::PythonRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut rr = PythonRub::new();
//! let b = Buildable::new(&mut rr, &vec!["rub".to_string(),
//!                                       "python".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use scm::hg::HgCommand;
use utils::usable_cores;
use utils::empty::to_opt;
use docopt::Docopt;
use std::default::Default;
use std::io::fs::PathExtensions;

static USAGE: &'static str = "python - Rust Builder

Usage:
    rub python [options] [<lifecycle>...]
    rub python (-h | --help)
    rub python --version

Options:
    -d --dir <dir>        Set the projects directory.
    -b --branch <branch>  Set the build branch. [default: master]
    -t --enable-test      Enable tests.
    -p --prefix <prefix>  Set the installation prefix.
    -u --url <url>        Set the SCM URL.
    -h --help             Show this usage.
    --target <target>     Target triples to build.
    --force-configure     Force the configure lifecycle to run.
    --version             Show python-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_target: Vec<String>,
    flag_force_configure: bool,
    flag_version: bool,
    flag_help: bool,
    arg_lifecycle: Vec<String>,
}

#[cfg(target_os = "linux")]
fn os_clean(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("sudo");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["make", "clean"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_clean(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("make");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["clean"]);
    cmd.exec(to_res())
}

#[cfg(target_os = "linux")]
fn os_install(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("sudo");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["sg", "cpython", "-c", "make install"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_install(cfg: &BuildConfig) -> Result<u8,u8> {
    let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
    let mut cmd = CommandExt::new("make");
    cmd.wd(&wd);
    cmd.header(true);
    cmd.args(&["install"]);
    cmd.exec(to_res())
}

/// python specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct PythonRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    target: Vec<String>,
    docs: bool,
    force_config: bool,
    ccache: bool,
    valgrind: bool,
}

impl PythonRub {
    /// Create a new default PythonRub
    pub fn new() -> PythonRub {
        Default::default()
    }
}

impl Buildable for PythonRub {
    /// Update the PythonRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate python_rub; fn main() {
    /// use buildable::Buildable;
    /// use python_rub::PythonRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = PythonRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "python".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut PythonRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());
        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.target = dargs.flag_target;
        self.force_config = dargs.flag_force_configure;

        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("python");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `PythonRub`.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate python_rub; fn main() {
    /// use buildable::Buildable;
    /// use python_rub::PythonRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = PythonRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "python".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("python", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for Python.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for Python dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `python` will be cloned from
    /// python.org automatically.  You can adjust where is is cloned from by
    /// using the `--url` flag at the command line.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "https://hg.python.org/cpython"
        } else {
            self.url.as_slice()
        };

        let mut hcmd = HgCommand::new();
        hcmd.wd(base.clone());
        hcmd.verbose(true);

        if !base.join(cfg.get_project()).exists() {
            hcmd.clone(Some(vec![u, "python"]), to_res())
        } else {
            Ok(0)
        }.and({
            hcmd.wd(base.join(cfg.get_project()));
            hcmd.pull(None, to_res())
        }).and(hcmd.update(None, to_res()))
    }

    /// Run `make clean` in the project directory.
    ///
    /// # Notes
    /// * This will fail if `configure` hasn't been run at least once, because
    /// no `Makefile` will exist.
    fn clean(&self) -> Result<u8,u8> {
        os_clean(&self.config)
    }

    /// Run `configure` in the project directory.
    ///
    /// # Examples
    /// <pre>
    /// rub python --force-configure
    /// rub python --prefix=/usr/local
    /// </pre>
    ///
    /// # Notes
    /// * If a `Makefile` doesn't exist or `--force-configure` was supplied then
    /// configure will run.  Otherwise, it will be skipped.
    /// * The default `configure` command is setup as:
    /// ``
    /// configure --prefix=/opt/python-<branch> --with-pydebug
    /// ``
    /// * You can adjust the `prefix` by using the `--prefix <x>` flag or the
    /// `--branch X` flag.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());

        if !wd.join("Makefile").exists() || self.force_config {
            let mut cmd = CommandExt::new("configure");
            cmd.wd(&wd);
            cmd.header(true);
            cmd.env("CXX", "/usr/bin/g++");
            let mut fullp = String::from_str("--prefix=");

            if self.prefix.is_empty() {
                fullp.push_str("/opt/python-");
                fullp.push_str(self.config.get_branch());
            } else {
                fullp.push_str(self.prefix.as_slice());
            }
            cmd.arg(fullp.as_slice());

            if cfg!(target_pointer_width = "32") {
                cmd.arg("--with-system-ffi");
            }

            cmd.arg("--with-pydebug");

            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make' in the project directory.
    ///
    /// Runs `make -j<X>` where X is the number of usable cores.
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("make");
        cmd.wd(&wd);
        cmd.arg(jobs.as_slice());
        cmd.header(true);
        cmd.exec(to_res())
    }

    /// Run 'python -m test -j<X>' in the project directory..
    fn test(&self) -> Result<u8,u8> {
        if self.config.get_test() {
            let cfg = &self.config;
            let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
            let mut jobs = String::from_str("-j");
            jobs.push_str(usable_cores().to_string().as_slice());
            let mut cmd = CommandExt::new("python");
            cmd.wd(&wd);
            cmd.args(&["-m", "test", jobs.as_slice()]);
            cmd.header(true);
            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make install' or 'sudo make install' in the project directory.
    ///
    /// Runs `make -j<X> install` where X is the number of usable cores.
    fn install(&self) -> Result<u8,u8> {
        os_install(&self.config)
    }

    /// Not implemented in this crate.
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} python-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::PythonRub;

    fn check_pr(pr: &PythonRub) {
        assert_eq!(pr.prefix, "");
        assert_eq!(pr.url, "");
        assert!(pr.target.is_empty());
        assert!(!pr.force_config);
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let proj = Path::new(env!("HOME").to_string()).join("projects");

        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), proj.as_str().unwrap());
        assert_eq!(bc.get_project(), "python");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let pr = PythonRub::new();
        check_pr(&pr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "python".to_string(),
                        "--version".to_string()];
        let mut pr = PythonRub::new();
        check_pr(&pr);
        let b = Buildable::new(&mut pr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "python".to_string(),
                        "-h".to_string()];
        let mut pr = PythonRub::new();
        check_pr(&pr);
        let b = Buildable::new(&mut pr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "python".to_string()];
        let mut pr = PythonRub::new();
        check_pr(&pr);
        let b = Buildable::new(&mut pr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "python".to_string(),
                        "scm".to_string()];
        let mut pr = PythonRub::new();
        check_pr(&pr);
        let b = Buildable::new(&mut pr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "python".to_string(),
                        "all".to_string()];
        let mut pr = PythonRub::new();
        check_pr(&pr);
        let b = Buildable::new(&mut pr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }
}
